### Crime µservice

This project implements a Spring Boot µservice that allows consumers to request crime data through a RESTful API.  The API exposes the following two endpoints:


Description: | Get 			a list of crime categories
-- | --
Method: | GET
URI: | _`/crime/categories`_
Response 			Code | _`200`_ 			if successful 		— 	_`404`_ 			if no data found


#### Example Response

```
{
   "categories" : [
      "all-crime",
      "burglary ",
      "anti-social-behaviour"
   ]
}

```

----

Description: | Get 			a list of crimes for the specified postcode and date
-- | --
Method: | GET
URI: | _`/crimes?postcode={postcode}&date={yyyy-mm}`_
Response 			Code: | _`400`_ 			if the post code or date is invalid 		— 	_`404`_ 			 if no data found 	—		_`200`_ 			if successful
 

#### Example Response

```
[
   {
      "category" : "anti-social-behaviour",
      "context" : "",
      "location" : {
         "latitude" : "52.640961",
         "longitude" : "-1.126371",
         "postcode" : "TF34NT"
      },
      "month" : "2017-01",
      "outcome_status" : null
   },
   {
      "category" : "burglary ",
      "context" : "",
      "location" : {
         "latitude" : "51.640961",
         "longitude" : "-1.166771",
         "postcode" : "TF34NT"
      },
      "month" : "2018-08",
      "outcome_status" : null
   }
]
```

----

### Usage

1. Change directory to the project's root directory:
    _`cd /path/to/project/root`_
2. To build and run from the command line:
    _`mvn package spring-boot:run`_
3. To retrieve crime data based on location<sup>_1_</sup> and date:
    _` curl "http://localhost:8080/crimes?postcode=TF3+4NT&date=2020-02"`_ 
4. To retrieve a list of categories of crimes:
    _`curl "http://localhost:8080/crime/categories"`_
    
    



<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
--

<br />
<sup><sup>1</sup> You MUST URL-encode any spaces in post codes. Use either the '+' symbol (TF3+4NB) or '20%' (TF3%204NB ) in place of spaces. Better still, don't use any of those (TF34NB ).</sup>
