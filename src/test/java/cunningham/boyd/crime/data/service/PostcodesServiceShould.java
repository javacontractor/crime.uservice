package cunningham.boyd.crime.data.service;

import cunningham.boyd.crime.data.Boydingham;
import cunningham.boyd.crime.data.client.model.postcodes.Postcodes;
import cunningham.boyd.crime.data.client.model.postcodes.Result;
import cunningham.boyd.crime.data.model.crime.PostcodeValidation;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.BDDAssertions.*;

/**
 * @author boyd@sun-certified.com
 */
@SpringBootTest(classes = Boydingham.class)
class PostcodesServiceShould {

    @Autowired
    private PostcodesService systemUnderTest;

    private static final Logger LOG = LoggerFactory.getLogger(PostcodesServiceShould.class);

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void loadContext() {
        assertThat(systemUnderTest).isNotNull();
    }

    @Test
    void justWork() {

        /* given */
        var plaza2 = "TF3 4NT";

        var expectedResult = new Postcodes()
                .withStatus(200)
                .withResult(new Result().withParish("Lawley and Overdale")
                        .withLatitude(52.680131).withLongitude(-2.445915));

        /* when */
        var actualResult = systemUnderTest.lookup(plaza2);

        /* then */
        assertThat(actualResult.getStatus()).isEqualTo(expectedResult.getStatus());
        assertThat(actualResult.getResult().getLatitude()).isEqualTo(expectedResult.getResult().getLatitude());
        assertThat(actualResult.getResult().getLongitude()).isEqualTo(expectedResult.getResult().getLongitude());
        assertThat(actualResult.getResult().getParish()).isEqualTo(expectedResult.getResult().getParish());
    }

    @Test
    void detectInvalidPostCodes() {

        /* given */
        String plaza1 = "660 38";

        String plaza2 = "TF3 4NT";

        PostcodeValidation expectedValidation = new PostcodeValidation().withStatus(200).withResult(false);

        /* when */
        PostcodeValidation actualValidation = systemUnderTest.validate(plaza1);

        /* then */
        assertThat(actualValidation).isEqualTo(expectedValidation);
    }

    @Test
    void detectValidPostCodes() {

        /* given */
        String plaza2 = "TF3 4NT";

        PostcodeValidation expectedValidation = new PostcodeValidation().withStatus(200).withResult(true);

        /* when */
        PostcodeValidation actualValidation = systemUnderTest.validate(plaza2);

        /* then */
        assertThat(actualValidation).isEqualTo(expectedValidation);
    }
}