package cunningham.boyd.crime.data.service;

import cunningham.boyd.crime.data.client.model.crime.Crime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.BDDAssertions.*;

/**
 * @author boyd@sun-certified.com
 */
@SpringBootTest
class CrimesAtLocationServiceShould {

    @Autowired
    private CrimesAtLocationService systemUnderTest;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void loadContext() {
        assertThat(systemUnderTest).isNotNull();
    }

    @Test
    void justWork() {

        /* given */
        var latitude = 52.680131;
        var longitude = -2.445915;
        var category = "anti-social-behaviour";
        var locationType = "Force";
        var date = "2020-02";
        var streetName = "On or near Ironmasters Way";

        var expectedCrime = new Crime().withCategory(category).withLocationType(locationType);

        /* when */
        var actualCrimes = systemUnderTest.locateCrimes(date, latitude, longitude);

        var hasAntiSocials = actualCrimes.stream().filter( crime -> crime.getCategory() != null && !crime.getCategory().isEmpty()).anyMatch(crime -> crime.getCategory().equals(category));

        var isNearIronMasters  = actualCrimes.stream().map( crime -> crime.getLocation()).anyMatch(location -> location.getStreet() != null && location.getStreet().getName().equals(streetName));

        /* then */
        assertThat(actualCrimes).isNotNull();
        assertThat(actualCrimes).isNotEmpty();
        assertThat(actualCrimes.size()).isEqualTo(4);
        assertThat(hasAntiSocials).isTrue();
        assertThat(isNearIronMasters).isTrue();
    }
}