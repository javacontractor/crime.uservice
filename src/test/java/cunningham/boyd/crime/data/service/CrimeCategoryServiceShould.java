package cunningham.boyd.crime.data.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cunningham.boyd.crime.data.Boydingham;
import cunningham.boyd.crime.data.model.crime.CrimeCategory;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.BDDAssertions.*;

/**
 * @author boyd@sun-certified.com
 */
@SpringBootTest( classes = Boydingham.class)
class CrimeCategoryServiceShould {

    private static final Logger LOG = LoggerFactory.getLogger(CrimeCategoryServiceShould.class);

    @Autowired
    private CrimeCategoryService systemUnderTest;

    private String expectedCrimeCatJSON = "[{\"url\":\"all-crime\",\"name\":\"All crime\"},{\"url\":\"anti-social-behaviour\",\"name\":\"Anti-social behaviour\"},{\"url\":\"bicycle-theft\",\"name\":\"Bicycle theft\"},{\"url\":\"burglary\",\"name\":\"Burglary\"},{\"url\":\"criminal-damage-arson\",\"name\":\"Criminal damage and arson\"},{\"url\":\"drugs\",\"name\":\"Drugs\"},{\"url\":\"other-theft\",\"name\":\"Other theft\"},{\"url\":\"possession-of-weapons\",\"name\":\"Possession of weapons\"},{\"url\":\"public-order\",\"name\":\"Public order\"},{\"url\":\"robbery\",\"name\":\"Robbery\"},{\"url\":\"shoplifting\",\"name\":\"Shoplifting\"},{\"url\":\"theft-from-the-person\",\"name\":\"Theft from the person\"},{\"url\":\"vehicle-crime\",\"name\":\"Vehicle crime\"},{\"url\":\"violent-crime\",\"name\":\"Violence and sexual offences\"},{\"url\":\"other-crime\",\"name\":\"Other crime\"}]";

    private ObjectMapper jackson = new ObjectMapper();

    @BeforeEach
    void setUp() {
    }

    @Test
    void loadContext() {
        assertThat(systemUnderTest).isNotNull();
    }

    @Test
    void justWork() throws Exception {

        /* given */
        List<LinkedHashMap<Object, Object>> expectedCrimeCats = jackson.readValue(expectedCrimeCatJSON, List.class);

        /* when */
        List<CrimeCategory> actualCrimeCats = systemUnderTest.allCategories();

        /* then */
        assertThat(actualCrimeCats).isNotNull();

        assertThat(actualCrimeCats.size()).isEqualTo(expectedCrimeCats.size());

        List<String> actualCatList = systemUnderTest.categorize(actualCrimeCats);

        List<String> expectedCatList =  expectedCrimeCats.stream().map(hashMap -> hashMap.get("url")).map(Objects::toString).collect(toList());

        assertThat(actualCatList).containsAll(expectedCatList);

        actualCatList.forEach( cat -> LOG.info(cat));

    }
}