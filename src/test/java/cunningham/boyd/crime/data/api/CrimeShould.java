package cunningham.boyd.crime.data.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import cunningham.boyd.crime.data.Boydingham;
import cunningham.boyd.crime.data.client.model.crime.Crime;
import cunningham.boyd.crime.data.client.model.crime.Location;
import cunningham.boyd.crime.data.client.model.postcodes.Postcodes;
import cunningham.boyd.crime.data.client.model.postcodes.Result;
import cunningham.boyd.crime.data.model.crime.CrimeCategory;
import cunningham.boyd.crime.data.model.crime.PostcodeValidation;
import cunningham.boyd.crime.data.service.CrimeCategoryService;
import cunningham.boyd.crime.data.service.CrimesAtLocationService;
import cunningham.boyd.crime.data.service.PostcodesService;
import cunningham.boyd.crime.data.support.PropMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.net.URI;
import java.net.URL;
import java.util.List;

import static java.lang.String.format;
import static org.assertj.core.api.BDDAssertions.*;
import static org.mockito.BDDMockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * @author boyd@sun-certified.com
 */
@SpringBootTest(webEnvironment =  SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Boydingham.class)
@AutoConfigureMockMvc
@ExtendWith(SpringExtension.class)
class CrimeShould {

    private static final Logger LOG = LoggerFactory.getLogger(CrimeShould.class);
    @MockBean
    private Postcodes postcodes;
    @MockBean
    private Result result;
    @Autowired
    private MockMvc systemUnderTest;

    @MockBean
    private CrimeCategoryService categoryService;

    @MockBean
    private CrimesAtLocationService locationService;

    @MockBean
    private PostcodesService postcodesService;

    @Autowired
    private PropMapper converter;

    private URI server;

    private int port;

    private ObjectMapper jackson;

    @MockBean
    private Crime clientCrime;
    @MockBean
    private Location clientLoc;

    @BeforeEach
    void setUp() throws Exception {
        server = new URL("http://localhost:" + port).toURI();
        jackson = new ObjectMapper();
    }

    @Test
    void loadContext() {
        assertThat(systemUnderTest).isNotNull();
        assertThat(server).isNotNull();
    }

    @Test
    void respondWithA200StatusForHappyPath() throws Exception {

        /* given */
        given(categoryService.categorize(anyList())).willReturn(List.of( "foo", "bar", "zlutf"));

        /* when */
        var actualResult = systemUnderTest.perform(get("/crime/categories")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("categories[0]").value("foo"))
                .andExpect(jsonPath("categories[2]").value("zlutf"))
                .andExpect(jsonPath("categories[1]").value("bar"))
                .andReturn();

        /* then */
        String actualResponseJSON = actualResult.getResponse().getContentAsString();

        CrimeCategory actualResponse = jackson.readValue(actualResponseJSON, CrimeCategory.class);

        LOG.info(" Result as JSON: {}", actualResponseJSON );

        LOG.info(" Result as POJO: {}", actualResponse );

        assertThat(actualResponseJSON).isNotNull();
    }

    @Test
    void respondWithA404StatusForNoDataFoundPath() throws Exception {

        /* given */
        given(categoryService.categorize(anyList())).willReturn(List.of());

        /* when */
        var actualResult = systemUnderTest.perform(get("/crime/categories")
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isNotFound())
/*                .andExpect(content().contentType(APPLICATION_JSON))
                .andExpect(jsonPath("categories[0]").value("foo"))
                .andExpect(jsonPath("categories[2]").value("zlutf"))
                .andExpect(jsonPath("categories[1]").value("bar"))*/
                .andReturn();

        /* then */
        String actualResponseJSON = actualResult.getResponse().getContentAsString();
/*

        CrimeCategory actualResponse = jackson.readValue(actualResponseJSON, CrimeCategory.class);
*/

        LOG.info(" Result as JSON: {}", actualResponseJSON );
/*

        LOG.info(" Result as POJO: {}", actualResponse );
*/

        assertThat(actualResponseJSON).isNotNull();
    }

    @Test
    void respondWithA200StatusForCrimesHappyPath() throws Exception {

        /* given */

        var latitude = 52.680131;
        var longitude = -2.445915;
        var plaza2 = "TF3 4NT";
        var date = "2020-02";

        given(postcodesService.lookup(anyString())).willReturn(postcodes);
        given(postcodesService.validate(anyString())).willReturn(new PostcodeValidation().withResult(true).withStatus(200));
        given(postcodes.getStatus()).willReturn(200);
        given(postcodes.getResult()).willReturn(result);
        given(result.getLatitude()).willReturn(latitude);
        given(result.getLongitude()).willReturn(longitude);
        given(locationService.locateCrimes(anyString(),anyDouble(),anyDouble())).willReturn(List.of(clientCrime, clientCrime));
        given(clientCrime.getMonth()).willReturn(date);
        given(clientCrime.getLocation()).willReturn(clientLoc);
        given(clientLoc.getLatitude()).willReturn(latitude + "");
        given(clientLoc.getLongitude()).willReturn(longitude + "");
        given(clientCrime.getCategory()).willReturn("anti-social-behavior", "burglary");

        /* when */
        var actualResult = systemUnderTest.perform(get(format("/crimes?postcode=%s&date=%s", plaza2, date))
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(APPLICATION_JSON))
                /*                .andExpect(jsonPath("category").value("foo"))
                                             .andExpect(jsonPath("categories[2]").value("zlutf"))
                                              .andExpect(jsonPath("categories[1]").value("bar"))*/
                .andReturn();

        /* then */
        String actualResponseJSON = actualResult.getResponse().getContentAsString();

        List<cunningham.boyd.crime.data.model.crime.Crime> actualResponse = jackson.readValue(actualResponseJSON, List.class);

        LOG.info(" Result as JSON: {}", actualResponseJSON );

        LOG.info(" Result as POJO: {}", actualResponse );

        assertThat(actualResponseJSON).isNotNull();
    }


    @Test
    void respondWithA400StatusForSpacesInPostCode() throws Exception {

        /* given */

        var latitude = 52.680131;
        var longitude = -2.445915;
        var leclaire = "660 38";
        var date = "2020-02";

        given(postcodesService.lookup(anyString())).willReturn(postcodes);
        given(postcodesService.validate(anyString())).willReturn(new PostcodeValidation().withResult(false).withStatus(200));
        given(postcodes.getStatus()).willReturn(200);
        given(postcodes.getResult()).willReturn(result);
        given(result.getLatitude()).willReturn(latitude);
        given(result.getLongitude()).willReturn(longitude);
        given(locationService.locateCrimes(anyString(),anyDouble(),anyDouble())).willReturn(List.of(clientCrime, clientCrime));
        given(clientCrime.getMonth()).willReturn(date);
        given(clientCrime.getLocation()).willReturn(clientLoc);
        given(clientLoc.getLatitude()).willReturn(latitude + "");
        given(clientLoc.getLongitude()).willReturn(longitude + "");
        given(clientCrime.getCategory()).willReturn("anti-social-behavior", "burglary");

        /* when */
        var actualResult = systemUnderTest.perform(get(format("/crimes?postcode=%s&date=%s", leclaire, date))
                .accept(APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(APPLICATION_JSON))
                /*                .andExpect(jsonPath("category").value("foo"))
                                             .andExpect(jsonPath("categor
                                             ies[2]").value("zlutf"))
                                              .andExpect(jsonPath("categories[1]").value("bar"))*/
                .andReturn();

        /* then */
        String actualResponseJSON = actualResult.getResponse().getContentAsString();

        List<cunningham.boyd.crime.data.model.crime.Crime> actualResponse = jackson.readValue(actualResponseJSON, List.class);

        LOG.info(" Result as JSON: {}", actualResponseJSON );

        LOG.info(" Result as POJO: {}", actualResponse );

        assertThat(actualResponseJSON).isNotNull();
    }
}