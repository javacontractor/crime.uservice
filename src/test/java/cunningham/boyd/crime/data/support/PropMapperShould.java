package cunningham.boyd.crime.data.support;

import cunningham.boyd.crime.data.model.crime.CrimeCategory;
import cunningham.boyd.crime.data.model.crime.Location;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.BDDAssertions.*;

/**
 * @author boyd@sun-certified.com
 */
class PropMapperShould {

    private PropMapper systemUnderTest;

    @BeforeEach
    void setUp() {
        systemUnderTest = new PropMapper();
    }

    @Test
    void copySameNameSimpleProperties() {

        /* given */

        var latitude = "52.680131";
        var longitude = "-2.445915";
        var plaza2 = "TF3 4NT";

        var svrSideLoc = new Location().withPostcode(plaza2);

        var clientSideLoc = new cunningham.boyd.crime.data.client.model.crime.Location().withLatitude(latitude).withLongitude(longitude);

        /* when */

        Location copiedLoc = systemUnderTest.map(clientSideLoc, svrSideLoc);

        /* then */
        assertThat(svrSideLoc.getLatitude()).isEqualTo(latitude);
        assertThat(svrSideLoc.getLongitude()).isEqualTo(longitude);
        assertThat(svrSideLoc.getPostcode()).isEqualTo(plaza2);

    }

    @Test
    void copyComplexPropertiesByName() {

        /* given */
        var crimeCatsList = List.of("foo", "bar", "bffrt");

        var crimeCats = new CrimeCategory();

        assertThat(crimeCats.getCategories()).isNull();

        /* when */
        var copiedCats = systemUnderTest.map(crimeCatsList, crimeCats);

        /* then */
        assertThat(crimeCats.getCategories()).isNotNull();

        assertThat(crimeCats.getCategories().size()).isEqualTo(crimeCatsList.size());

        assertThat(crimeCats.getCategories()).containsAll(crimeCatsList);
    }
}