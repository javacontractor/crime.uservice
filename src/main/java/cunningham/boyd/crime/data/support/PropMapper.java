package cunningham.boyd.crime.data.support;

import cunningham.boyd.crime.data.model.crime.Crime;
import cunningham.boyd.crime.data.model.crime.CrimeCategory;
import cunningham.boyd.crime.data.model.crime.Location;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * A Utility class with methods that copy values of properties
 * of an instance of one type to the properties of
 * an intance of a different type.
 *
 * @author boyd@sun-certified.com
 */
@Service
public class PropMapper {

    /**
     * The logger.
     */
    private static final Logger LOG = LoggerFactory.getLogger(PropMapper.class);

    /**
     * Copy similar-name properties of the first class to those
     * of the second class.
     *
     * @param clientSideLoc The source from which to copy properties.
     * @param svrSideLoc The destination to which to copy properties.
     * @return An instance of the same type as the destination, with
     * the values of properties in the {@code clientSideCrime}.
     */
    public Location map(final cunningham.boyd
            .crime.data.client.model.crime.Location
                                clientSideLoc, final Location svrSideLoc) {
        try {
            svrSideLoc.setLatitude(clientSideLoc.getLatitude());
            svrSideLoc.setLongitude(clientSideLoc.getLongitude());
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }
        return svrSideLoc;
    }

    /**
     * Copy similar-name properties of the first class to those
     * of the second class.
     *
     * @param crimeCatsList The source from which to copy properties.
     * @param crimeCats The destination to which to copy properties.
     * @return An instance of the same type as the destination, with
     * the values of properties in the {@code clientSideCrime}.
     */
    public CrimeCategory map(final List<String> crimeCatsList,
                             final CrimeCategory crimeCats) {

        try {
            BeanUtils.copyProperty(crimeCats, "categories", crimeCatsList);
        } catch (IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            LOG.error(e.getMessage(), e);
        }
        return crimeCats;
    }

    /**
     * Copy similar-name properties of the first class to those
     * of the second class.
     *
     * @param clientSideCrime The source from which to copy properties.
     * @param svrSideCrime The destination to which to copy properties.
     * @return An instance of the same type as the destination, with
     * the values of properties in the {@code clientSideCrime}.
     */
    public Crime map(final cunningham.boyd.crime.data.client.model.crime.Crime
                             clientSideCrime, final Crime svrSideCrime) {
        try {
            if (svrSideCrime.getOutcomeStatus() != null) {
                BeanUtils.copyProperties(svrSideCrime.getOutcomeStatus(),
                        clientSideCrime.getOutcomeStatus());
            }

            svrSideCrime.setMonth(clientSideCrime.getMonth());
            svrSideCrime.setCategory(clientSideCrime.getCategory());

            cunningham.boyd.crime.data.client.model.crime.Location
                    clientSideCrimeLocation =
                    clientSideCrime.getLocation();
            Location svrSideLocation =
                    map(clientSideCrimeLocation, new Location());
            svrSideCrime.setLocation(svrSideLocation);
        } catch (IllegalAccessException e) {
            LOG.error(e.getMessage(), e);
        } catch (InvocationTargetException e) {
            LOG.error(e.getMessage(), e);
        }
        return svrSideCrime;
    }
}
