package cunningham.boyd.crime.data.service;

import cunningham.boyd.crime.data.conf.BoydinghamConfiguration;
import cunningham.boyd.crime.data.client.model.postcodes.Postcodes;
import cunningham.boyd.crime.data.model.crime.PostcodeValidation;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author boyd@sun-certified.com
 */
@Service
@FeignClient(value = "postcodes",
        url = "https://api.postcodes.io/postcodes",
        configuration = BoydinghamConfiguration.class)
public interface PostcodesService {

    @RequestMapping(method = RequestMethod.GET, value = "/{postCode}",
            produces = "application/json")
    Postcodes lookup(@PathVariable("postCode") String postCode);

    @RequestMapping(method = RequestMethod.GET, value = "/{postCode}/validate",
            produces = "application/json")
    PostcodeValidation validate(@PathVariable("postCode") String postCode);

}
