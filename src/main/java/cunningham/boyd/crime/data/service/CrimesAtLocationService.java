package cunningham.boyd.crime.data.service;

import cunningham.boyd.crime.data.conf.BoydinghamConfiguration;
import cunningham.boyd.crime.data.client.model.crime.Crime;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author boyd@sun-certified.com
 */
@Service
@FeignClient(value = "crimesatlocaion",
        url = "https://data.police.uk/api/crimes-at-location/",
        configuration = BoydinghamConfiguration.class)
public interface CrimesAtLocationService {

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    List<Crime> locateCrimes(@RequestParam(required = true, name = "date")
                                     String date,
                             @RequestParam(required = true, name = "lat")
                                     Double latitude,
                             @RequestParam(required = true, name = "lng")
                                     Double longitude);
}
