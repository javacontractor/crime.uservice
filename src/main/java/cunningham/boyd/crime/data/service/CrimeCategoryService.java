package cunningham.boyd.crime.data.service;

import cunningham.boyd.crime.data.conf.BoydinghamConfiguration;
import cunningham.boyd.crime.data.model.crime.CrimeCategory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

/**
 * @author boyd@sun-certified.com
 */
@Service
@FeignClient(value = "crimecategories",
        url = "https://data.police.uk/api/crime-categories",
        configuration = BoydinghamConfiguration.class)
public interface CrimeCategoryService {

    @GetMapping(path = "", produces = "application/json")
    List<CrimeCategory> allCategories();

    /**
     * Covert the given list of {@link CrimeCategory} to a
     * list of {@link String}s.
     *
     * @param crimeCategories The source list.
     *
     * @return A categorized list of crimes.
     */
    default List<String> categorize(List<CrimeCategory> crimeCategories) {
        return crimeCategories.stream()
                .map(crimeCategory -> crimeCategory.getAdditionalProperties()
                        .get("url"))
                .map(Objects::toString).collect(toList());
    }
}
