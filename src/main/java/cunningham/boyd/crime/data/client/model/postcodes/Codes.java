
package cunningham.boyd.crime.data.client.model.postcodes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "admin_district",
    "admin_county",
    "admin_ward",
    "parish",
    "parliamentary_constituency",
    "ccg",
    "ccg_id",
    "ced",
    "nuts",
    "lsoa",
    "msoa",
    "lau2"
})
@Generated("jsonschema2pojo")
public class Codes implements Serializable
{

    @JsonProperty("admin_district")
    private String adminDistrict;
    @JsonProperty("admin_county")
    private String adminCounty;
    @JsonProperty("admin_ward")
    private String adminWard;
    @JsonProperty("parish")
    private String parish;
    @JsonProperty("parliamentary_constituency")
    private String parliamentaryConstituency;
    @JsonProperty("ccg")
    private String ccg;
    @JsonProperty("ccg_id")
    private String ccgId;
    @JsonProperty("ced")
    private String ced;
    @JsonProperty("nuts")
    private String nuts;
    @JsonProperty("lsoa")
    private String lsoa;
    @JsonProperty("msoa")
    private String msoa;
    @JsonProperty("lau2")
    private String lau2;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 871311566802767912L;

    @JsonProperty("admin_district")
    public String getAdminDistrict() {
        return adminDistrict;
    }

    @JsonProperty("admin_district")
    public void setAdminDistrict(String adminDistrict) {
        this.adminDistrict = adminDistrict;
    }

    public Codes withAdminDistrict(String adminDistrict) {
        this.adminDistrict = adminDistrict;
        return this;
    }

    @JsonProperty("admin_county")
    public String getAdminCounty() {
        return adminCounty;
    }

    @JsonProperty("admin_county")
    public void setAdminCounty(String adminCounty) {
        this.adminCounty = adminCounty;
    }

    public Codes withAdminCounty(String adminCounty) {
        this.adminCounty = adminCounty;
        return this;
    }

    @JsonProperty("admin_ward")
    public String getAdminWard() {
        return adminWard;
    }

    @JsonProperty("admin_ward")
    public void setAdminWard(String adminWard) {
        this.adminWard = adminWard;
    }

    public Codes withAdminWard(String adminWard) {
        this.adminWard = adminWard;
        return this;
    }

    @JsonProperty("parish")
    public String getParish() {
        return parish;
    }

    @JsonProperty("parish")
    public void setParish(String parish) {
        this.parish = parish;
    }

    public Codes withParish(String parish) {
        this.parish = parish;
        return this;
    }

    @JsonProperty("parliamentary_constituency")
    public String getParliamentaryConstituency() {
        return parliamentaryConstituency;
    }

    @JsonProperty("parliamentary_constituency")
    public void setParliamentaryConstituency(String parliamentaryConstituency) {
        this.parliamentaryConstituency = parliamentaryConstituency;
    }

    public Codes withParliamentaryConstituency(String parliamentaryConstituency) {
        this.parliamentaryConstituency = parliamentaryConstituency;
        return this;
    }

    @JsonProperty("ccg")
    public String getCcg() {
        return ccg;
    }

    @JsonProperty("ccg")
    public void setCcg(String ccg) {
        this.ccg = ccg;
    }

    public Codes withCcg(String ccg) {
        this.ccg = ccg;
        return this;
    }

    @JsonProperty("ccg_id")
    public String getCcgId() {
        return ccgId;
    }

    @JsonProperty("ccg_id")
    public void setCcgId(String ccgId) {
        this.ccgId = ccgId;
    }

    public Codes withCcgId(String ccgId) {
        this.ccgId = ccgId;
        return this;
    }

    @JsonProperty("ced")
    public String getCed() {
        return ced;
    }

    @JsonProperty("ced")
    public void setCed(String ced) {
        this.ced = ced;
    }

    public Codes withCed(String ced) {
        this.ced = ced;
        return this;
    }

    @JsonProperty("nuts")
    public String getNuts() {
        return nuts;
    }

    @JsonProperty("nuts")
    public void setNuts(String nuts) {
        this.nuts = nuts;
    }

    public Codes withNuts(String nuts) {
        this.nuts = nuts;
        return this;
    }

    @JsonProperty("lsoa")
    public String getLsoa() {
        return lsoa;
    }

    @JsonProperty("lsoa")
    public void setLsoa(String lsoa) {
        this.lsoa = lsoa;
    }

    public Codes withLsoa(String lsoa) {
        this.lsoa = lsoa;
        return this;
    }

    @JsonProperty("msoa")
    public String getMsoa() {
        return msoa;
    }

    @JsonProperty("msoa")
    public void setMsoa(String msoa) {
        this.msoa = msoa;
    }

    public Codes withMsoa(String msoa) {
        this.msoa = msoa;
        return this;
    }

    @JsonProperty("lau2")
    public String getLau2() {
        return lau2;
    }

    @JsonProperty("lau2")
    public void setLau2(String lau2) {
        this.lau2 = lau2;
    }

    public Codes withLau2(String lau2) {
        this.lau2 = lau2;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Codes withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Codes.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("adminDistrict");
        sb.append('=');
        sb.append(((this.adminDistrict == null)?"<null>":this.adminDistrict));
        sb.append(',');
        sb.append("adminCounty");
        sb.append('=');
        sb.append(((this.adminCounty == null)?"<null>":this.adminCounty));
        sb.append(',');
        sb.append("adminWard");
        sb.append('=');
        sb.append(((this.adminWard == null)?"<null>":this.adminWard));
        sb.append(',');
        sb.append("parish");
        sb.append('=');
        sb.append(((this.parish == null)?"<null>":this.parish));
        sb.append(',');
        sb.append("parliamentaryConstituency");
        sb.append('=');
        sb.append(((this.parliamentaryConstituency == null)?"<null>":this.parliamentaryConstituency));
        sb.append(',');
        sb.append("ccg");
        sb.append('=');
        sb.append(((this.ccg == null)?"<null>":this.ccg));
        sb.append(',');
        sb.append("ccgId");
        sb.append('=');
        sb.append(((this.ccgId == null)?"<null>":this.ccgId));
        sb.append(',');
        sb.append("ced");
        sb.append('=');
        sb.append(((this.ced == null)?"<null>":this.ced));
        sb.append(',');
        sb.append("nuts");
        sb.append('=');
        sb.append(((this.nuts == null)?"<null>":this.nuts));
        sb.append(',');
        sb.append("lsoa");
        sb.append('=');
        sb.append(((this.lsoa == null)?"<null>":this.lsoa));
        sb.append(',');
        sb.append("msoa");
        sb.append('=');
        sb.append(((this.msoa == null)?"<null>":this.msoa));
        sb.append(',');
        sb.append("lau2");
        sb.append('=');
        sb.append(((this.lau2 == null)?"<null>":this.lau2));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.lau2 == null)? 0 :this.lau2 .hashCode()));
        result = ((result* 31)+((this.ccgId == null)? 0 :this.ccgId.hashCode()));
        result = ((result* 31)+((this.ced == null)? 0 :this.ced.hashCode()));
        result = ((result* 31)+((this.adminWard == null)? 0 :this.adminWard.hashCode()));
        result = ((result* 31)+((this.ccg == null)? 0 :this.ccg.hashCode()));
        result = ((result* 31)+((this.parliamentaryConstituency == null)? 0 :this.parliamentaryConstituency.hashCode()));
        result = ((result* 31)+((this.lsoa == null)? 0 :this.lsoa.hashCode()));
        result = ((result* 31)+((this.msoa == null)? 0 :this.msoa.hashCode()));
        result = ((result* 31)+((this.adminCounty == null)? 0 :this.adminCounty.hashCode()));
        result = ((result* 31)+((this.nuts == null)? 0 :this.nuts.hashCode()));
        result = ((result* 31)+((this.parish == null)? 0 :this.parish.hashCode()));
        result = ((result* 31)+((this.adminDistrict == null)? 0 :this.adminDistrict.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Codes) == false) {
            return false;
        }
        Codes rhs = ((Codes) other);
        return ((((((((((((((this.lau2 == rhs.lau2)||((this.lau2 != null)&&this.lau2 .equals(rhs.lau2)))&&((this.ccgId == rhs.ccgId)||((this.ccgId!= null)&&this.ccgId.equals(rhs.ccgId))))&&((this.ced == rhs.ced)||((this.ced!= null)&&this.ced.equals(rhs.ced))))&&((this.adminWard == rhs.adminWard)||((this.adminWard!= null)&&this.adminWard.equals(rhs.adminWard))))&&((this.ccg == rhs.ccg)||((this.ccg!= null)&&this.ccg.equals(rhs.ccg))))&&((this.parliamentaryConstituency == rhs.parliamentaryConstituency)||((this.parliamentaryConstituency!= null)&&this.parliamentaryConstituency.equals(rhs.parliamentaryConstituency))))&&((this.lsoa == rhs.lsoa)||((this.lsoa!= null)&&this.lsoa.equals(rhs.lsoa))))&&((this.msoa == rhs.msoa)||((this.msoa!= null)&&this.msoa.equals(rhs.msoa))))&&((this.adminCounty == rhs.adminCounty)||((this.adminCounty!= null)&&this.adminCounty.equals(rhs.adminCounty))))&&((this.nuts == rhs.nuts)||((this.nuts!= null)&&this.nuts.equals(rhs.nuts))))&&((this.parish == rhs.parish)||((this.parish!= null)&&this.parish.equals(rhs.parish))))&&((this.adminDistrict == rhs.adminDistrict)||((this.adminDistrict!= null)&&this.adminDistrict.equals(rhs.adminDistrict))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))));
    }

}
