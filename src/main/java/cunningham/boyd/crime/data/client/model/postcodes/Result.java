
package cunningham.boyd.crime.data.client.model.postcodes;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "postcode",
    "quality",
    "eastings",
    "northings",
    "country",
    "nhs_ha",
    "longitude",
    "latitude",
    "european_electoral_region",
    "primary_care_trust",
    "region",
    "lsoa",
    "msoa",
    "incode",
    "outcode",
    "parliamentary_constituency",
    "admin_district",
    "parish",
    "admin_county",
    "admin_ward",
    "ced",
    "ccg",
    "nuts",
    "codes"
})
@Generated("jsonschema2pojo")
public class Result implements Serializable
{

    @JsonProperty("postcode")
    private String postcode;
    @JsonProperty("quality")
    private Integer quality;
    @JsonProperty("eastings")
    private Integer eastings;
    @JsonProperty("northings")
    private Integer northings;
    @JsonProperty("country")
    private String country;
    @JsonProperty("nhs_ha")
    private String nhsHa;
    @JsonProperty("longitude")
    private Double longitude;
    @JsonProperty("latitude")
    private Double latitude;
    @JsonProperty("european_electoral_region")
    private String europeanElectoralRegion;
    @JsonProperty("primary_care_trust")
    private String primaryCareTrust;
    @JsonProperty("region")
    private String region;
    @JsonProperty("lsoa")
    private String lsoa;
    @JsonProperty("msoa")
    private String msoa;
    @JsonProperty("incode")
    private String incode;
    @JsonProperty("outcode")
    private String outcode;
    @JsonProperty("parliamentary_constituency")
    private String parliamentaryConstituency;
    @JsonProperty("admin_district")
    private String adminDistrict;
    @JsonProperty("parish")
    private String parish;
    @JsonProperty("admin_county")
    private Object adminCounty;
    @JsonProperty("admin_ward")
    private String adminWard;
    @JsonProperty("ced")
    private Object ced;
    @JsonProperty("ccg")
    private String ccg;
    @JsonProperty("nuts")
    private String nuts;
    @JsonProperty("codes")
    private Codes codes;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = 2043803421196875417L;

    @JsonProperty("postcode")
    public String getPostcode() {
        return postcode;
    }

    @JsonProperty("postcode")
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Result withPostcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    @JsonProperty("quality")
    public Integer getQuality() {
        return quality;
    }

    @JsonProperty("quality")
    public void setQuality(Integer quality) {
        this.quality = quality;
    }

    public Result withQuality(Integer quality) {
        this.quality = quality;
        return this;
    }

    @JsonProperty("eastings")
    public Integer getEastings() {
        return eastings;
    }

    @JsonProperty("eastings")
    public void setEastings(Integer eastings) {
        this.eastings = eastings;
    }

    public Result withEastings(Integer eastings) {
        this.eastings = eastings;
        return this;
    }

    @JsonProperty("northings")
    public Integer getNorthings() {
        return northings;
    }

    @JsonProperty("northings")
    public void setNorthings(Integer northings) {
        this.northings = northings;
    }

    public Result withNorthings(Integer northings) {
        this.northings = northings;
        return this;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    public Result withCountry(String country) {
        this.country = country;
        return this;
    }

    @JsonProperty("nhs_ha")
    public String getNhsHa() {
        return nhsHa;
    }

    @JsonProperty("nhs_ha")
    public void setNhsHa(String nhsHa) {
        this.nhsHa = nhsHa;
    }

    public Result withNhsHa(String nhsHa) {
        this.nhsHa = nhsHa;
        return this;
    }

    @JsonProperty("longitude")
    public Double getLongitude() {
        return longitude;
    }

    @JsonProperty("longitude")
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Result withLongitude(Double longitude) {
        this.longitude = longitude;
        return this;
    }

    @JsonProperty("latitude")
    public Double getLatitude() {
        return latitude;
    }

    @JsonProperty("latitude")
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Result withLatitude(Double latitude) {
        this.latitude = latitude;
        return this;
    }

    @JsonProperty("european_electoral_region")
    public String getEuropeanElectoralRegion() {
        return europeanElectoralRegion;
    }

    @JsonProperty("european_electoral_region")
    public void setEuropeanElectoralRegion(String europeanElectoralRegion) {
        this.europeanElectoralRegion = europeanElectoralRegion;
    }

    public Result withEuropeanElectoralRegion(String europeanElectoralRegion) {
        this.europeanElectoralRegion = europeanElectoralRegion;
        return this;
    }

    @JsonProperty("primary_care_trust")
    public String getPrimaryCareTrust() {
        return primaryCareTrust;
    }

    @JsonProperty("primary_care_trust")
    public void setPrimaryCareTrust(String primaryCareTrust) {
        this.primaryCareTrust = primaryCareTrust;
    }

    public Result withPrimaryCareTrust(String primaryCareTrust) {
        this.primaryCareTrust = primaryCareTrust;
        return this;
    }

    @JsonProperty("region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    public Result withRegion(String region) {
        this.region = region;
        return this;
    }

    @JsonProperty("lsoa")
    public String getLsoa() {
        return lsoa;
    }

    @JsonProperty("lsoa")
    public void setLsoa(String lsoa) {
        this.lsoa = lsoa;
    }

    public Result withLsoa(String lsoa) {
        this.lsoa = lsoa;
        return this;
    }

    @JsonProperty("msoa")
    public String getMsoa() {
        return msoa;
    }

    @JsonProperty("msoa")
    public void setMsoa(String msoa) {
        this.msoa = msoa;
    }

    public Result withMsoa(String msoa) {
        this.msoa = msoa;
        return this;
    }

    @JsonProperty("incode")
    public String getIncode() {
        return incode;
    }

    @JsonProperty("incode")
    public void setIncode(String incode) {
        this.incode = incode;
    }

    public Result withIncode(String incode) {
        this.incode = incode;
        return this;
    }

    @JsonProperty("outcode")
    public String getOutcode() {
        return outcode;
    }

    @JsonProperty("outcode")
    public void setOutcode(String outcode) {
        this.outcode = outcode;
    }

    public Result withOutcode(String outcode) {
        this.outcode = outcode;
        return this;
    }

    @JsonProperty("parliamentary_constituency")
    public String getParliamentaryConstituency() {
        return parliamentaryConstituency;
    }

    @JsonProperty("parliamentary_constituency")
    public void setParliamentaryConstituency(String parliamentaryConstituency) {
        this.parliamentaryConstituency = parliamentaryConstituency;
    }

    public Result withParliamentaryConstituency(String parliamentaryConstituency) {
        this.parliamentaryConstituency = parliamentaryConstituency;
        return this;
    }

    @JsonProperty("admin_district")
    public String getAdminDistrict() {
        return adminDistrict;
    }

    @JsonProperty("admin_district")
    public void setAdminDistrict(String adminDistrict) {
        this.adminDistrict = adminDistrict;
    }

    public Result withAdminDistrict(String adminDistrict) {
        this.adminDistrict = adminDistrict;
        return this;
    }

    @JsonProperty("parish")
    public String getParish() {
        return parish;
    }

    @JsonProperty("parish")
    public void setParish(String parish) {
        this.parish = parish;
    }

    public Result withParish(String parish) {
        this.parish = parish;
        return this;
    }

    @JsonProperty("admin_county")
    public Object getAdminCounty() {
        return adminCounty;
    }

    @JsonProperty("admin_county")
    public void setAdminCounty(Object adminCounty) {
        this.adminCounty = adminCounty;
    }

    public Result withAdminCounty(Object adminCounty) {
        this.adminCounty = adminCounty;
        return this;
    }

    @JsonProperty("admin_ward")
    public String getAdminWard() {
        return adminWard;
    }

    @JsonProperty("admin_ward")
    public void setAdminWard(String adminWard) {
        this.adminWard = adminWard;
    }

    public Result withAdminWard(String adminWard) {
        this.adminWard = adminWard;
        return this;
    }

    @JsonProperty("ced")
    public Object getCed() {
        return ced;
    }

    @JsonProperty("ced")
    public void setCed(Object ced) {
        this.ced = ced;
    }

    public Result withCed(Object ced) {
        this.ced = ced;
        return this;
    }

    @JsonProperty("ccg")
    public String getCcg() {
        return ccg;
    }

    @JsonProperty("ccg")
    public void setCcg(String ccg) {
        this.ccg = ccg;
    }

    public Result withCcg(String ccg) {
        this.ccg = ccg;
        return this;
    }

    @JsonProperty("nuts")
    public String getNuts() {
        return nuts;
    }

    @JsonProperty("nuts")
    public void setNuts(String nuts) {
        this.nuts = nuts;
    }

    public Result withNuts(String nuts) {
        this.nuts = nuts;
        return this;
    }

    @JsonProperty("codes")
    public Codes getCodes() {
        return codes;
    }

    @JsonProperty("codes")
    public void setCodes(Codes codes) {
        this.codes = codes;
    }

    public Result withCodes(Codes codes) {
        this.codes = codes;
        return this;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Result withAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Result.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("postcode");
        sb.append('=');
        sb.append(((this.postcode == null)?"<null>":this.postcode));
        sb.append(',');
        sb.append("quality");
        sb.append('=');
        sb.append(((this.quality == null)?"<null>":this.quality));
        sb.append(',');
        sb.append("eastings");
        sb.append('=');
        sb.append(((this.eastings == null)?"<null>":this.eastings));
        sb.append(',');
        sb.append("northings");
        sb.append('=');
        sb.append(((this.northings == null)?"<null>":this.northings));
        sb.append(',');
        sb.append("country");
        sb.append('=');
        sb.append(((this.country == null)?"<null>":this.country));
        sb.append(',');
        sb.append("nhsHa");
        sb.append('=');
        sb.append(((this.nhsHa == null)?"<null>":this.nhsHa));
        sb.append(',');
        sb.append("longitude");
        sb.append('=');
        sb.append(((this.longitude == null)?"<null>":this.longitude));
        sb.append(',');
        sb.append("latitude");
        sb.append('=');
        sb.append(((this.latitude == null)?"<null>":this.latitude));
        sb.append(',');
        sb.append("europeanElectoralRegion");
        sb.append('=');
        sb.append(((this.europeanElectoralRegion == null)?"<null>":this.europeanElectoralRegion));
        sb.append(',');
        sb.append("primaryCareTrust");
        sb.append('=');
        sb.append(((this.primaryCareTrust == null)?"<null>":this.primaryCareTrust));
        sb.append(',');
        sb.append("region");
        sb.append('=');
        sb.append(((this.region == null)?"<null>":this.region));
        sb.append(',');
        sb.append("lsoa");
        sb.append('=');
        sb.append(((this.lsoa == null)?"<null>":this.lsoa));
        sb.append(',');
        sb.append("msoa");
        sb.append('=');
        sb.append(((this.msoa == null)?"<null>":this.msoa));
        sb.append(',');
        sb.append("incode");
        sb.append('=');
        sb.append(((this.incode == null)?"<null>":this.incode));
        sb.append(',');
        sb.append("outcode");
        sb.append('=');
        sb.append(((this.outcode == null)?"<null>":this.outcode));
        sb.append(',');
        sb.append("parliamentaryConstituency");
        sb.append('=');
        sb.append(((this.parliamentaryConstituency == null)?"<null>":this.parliamentaryConstituency));
        sb.append(',');
        sb.append("adminDistrict");
        sb.append('=');
        sb.append(((this.adminDistrict == null)?"<null>":this.adminDistrict));
        sb.append(',');
        sb.append("parish");
        sb.append('=');
        sb.append(((this.parish == null)?"<null>":this.parish));
        sb.append(',');
        sb.append("adminCounty");
        sb.append('=');
        sb.append(((this.adminCounty == null)?"<null>":this.adminCounty));
        sb.append(',');
        sb.append("adminWard");
        sb.append('=');
        sb.append(((this.adminWard == null)?"<null>":this.adminWard));
        sb.append(',');
        sb.append("ced");
        sb.append('=');
        sb.append(((this.ced == null)?"<null>":this.ced));
        sb.append(',');
        sb.append("ccg");
        sb.append('=');
        sb.append(((this.ccg == null)?"<null>":this.ccg));
        sb.append(',');
        sb.append("nuts");
        sb.append('=');
        sb.append(((this.nuts == null)?"<null>":this.nuts));
        sb.append(',');
        sb.append("codes");
        sb.append('=');
        sb.append(((this.codes == null)?"<null>":this.codes));
        sb.append(',');
        sb.append("additionalProperties");
        sb.append('=');
        sb.append(((this.additionalProperties == null)?"<null>":this.additionalProperties));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

    @Override
    public int hashCode() {
        int result = 1;
        result = ((result* 31)+((this.country == null)? 0 :this.country.hashCode()));
        result = ((result* 31)+((this.codes == null)? 0 :this.codes.hashCode()));
        result = ((result* 31)+((this.ced == null)? 0 :this.ced.hashCode()));
        result = ((result* 31)+((this.ccg == null)? 0 :this.ccg.hashCode()));
        result = ((result* 31)+((this.latitude == null)? 0 :this.latitude.hashCode()));
        result = ((result* 31)+((this.parliamentaryConstituency == null)? 0 :this.parliamentaryConstituency.hashCode()));
        result = ((result* 31)+((this.msoa == null)? 0 :this.msoa.hashCode()));
        result = ((result* 31)+((this.adminCounty == null)? 0 :this.adminCounty.hashCode()));
        result = ((result* 31)+((this.parish == null)? 0 :this.parish.hashCode()));
        result = ((result* 31)+((this.longitude == null)? 0 :this.longitude.hashCode()));
        result = ((result* 31)+((this.adminWard == null)? 0 :this.adminWard.hashCode()));
        result = ((result* 31)+((this.postcode == null)? 0 :this.postcode.hashCode()));
        result = ((result* 31)+((this.eastings == null)? 0 :this.eastings.hashCode()));
        result = ((result* 31)+((this.europeanElectoralRegion == null)? 0 :this.europeanElectoralRegion.hashCode()));
        result = ((result* 31)+((this.lsoa == null)? 0 :this.lsoa.hashCode()));
        result = ((result* 31)+((this.nhsHa == null)? 0 :this.nhsHa.hashCode()));
        result = ((result* 31)+((this.quality == null)? 0 :this.quality.hashCode()));
        result = ((result* 31)+((this.primaryCareTrust == null)? 0 :this.primaryCareTrust.hashCode()));
        result = ((result* 31)+((this.nuts == null)? 0 :this.nuts.hashCode()));
        result = ((result* 31)+((this.outcode == null)? 0 :this.outcode.hashCode()));
        result = ((result* 31)+((this.adminDistrict == null)? 0 :this.adminDistrict.hashCode()));
        result = ((result* 31)+((this.northings == null)? 0 :this.northings.hashCode()));
        result = ((result* 31)+((this.incode == null)? 0 :this.incode.hashCode()));
        result = ((result* 31)+((this.additionalProperties == null)? 0 :this.additionalProperties.hashCode()));
        result = ((result* 31)+((this.region == null)? 0 :this.region.hashCode()));
        return result;
    }

    @Override
    public boolean equals(Object other) {
        if (other == this) {
            return true;
        }
        if ((other instanceof Result) == false) {
            return false;
        }
        Result rhs = ((Result) other);
        return ((((((((((((((((((((((((((this.country == rhs.country)||((this.country!= null)&&this.country.equals(rhs.country)))&&((this.codes == rhs.codes)||((this.codes!= null)&&this.codes.equals(rhs.codes))))&&((this.ced == rhs.ced)||((this.ced!= null)&&this.ced.equals(rhs.ced))))&&((this.ccg == rhs.ccg)||((this.ccg!= null)&&this.ccg.equals(rhs.ccg))))&&((this.latitude == rhs.latitude)||((this.latitude!= null)&&this.latitude.equals(rhs.latitude))))&&((this.parliamentaryConstituency == rhs.parliamentaryConstituency)||((this.parliamentaryConstituency!= null)&&this.parliamentaryConstituency.equals(rhs.parliamentaryConstituency))))&&((this.msoa == rhs.msoa)||((this.msoa!= null)&&this.msoa.equals(rhs.msoa))))&&((this.adminCounty == rhs.adminCounty)||((this.adminCounty!= null)&&this.adminCounty.equals(rhs.adminCounty))))&&((this.parish == rhs.parish)||((this.parish!= null)&&this.parish.equals(rhs.parish))))&&((this.longitude == rhs.longitude)||((this.longitude!= null)&&this.longitude.equals(rhs.longitude))))&&((this.adminWard == rhs.adminWard)||((this.adminWard!= null)&&this.adminWard.equals(rhs.adminWard))))&&((this.postcode == rhs.postcode)||((this.postcode!= null)&&this.postcode.equals(rhs.postcode))))&&((this.eastings == rhs.eastings)||((this.eastings!= null)&&this.eastings.equals(rhs.eastings))))&&((this.europeanElectoralRegion == rhs.europeanElectoralRegion)||((this.europeanElectoralRegion!= null)&&this.europeanElectoralRegion.equals(rhs.europeanElectoralRegion))))&&((this.lsoa == rhs.lsoa)||((this.lsoa!= null)&&this.lsoa.equals(rhs.lsoa))))&&((this.nhsHa == rhs.nhsHa)||((this.nhsHa!= null)&&this.nhsHa.equals(rhs.nhsHa))))&&((this.quality == rhs.quality)||((this.quality!= null)&&this.quality.equals(rhs.quality))))&&((this.primaryCareTrust == rhs.primaryCareTrust)||((this.primaryCareTrust!= null)&&this.primaryCareTrust.equals(rhs.primaryCareTrust))))&&((this.nuts == rhs.nuts)||((this.nuts!= null)&&this.nuts.equals(rhs.nuts))))&&((this.outcode == rhs.outcode)||((this.outcode!= null)&&this.outcode.equals(rhs.outcode))))&&((this.adminDistrict == rhs.adminDistrict)||((this.adminDistrict!= null)&&this.adminDistrict.equals(rhs.adminDistrict))))&&((this.northings == rhs.northings)||((this.northings!= null)&&this.northings.equals(rhs.northings))))&&((this.incode == rhs.incode)||((this.incode!= null)&&this.incode.equals(rhs.incode))))&&((this.additionalProperties == rhs.additionalProperties)||((this.additionalProperties!= null)&&this.additionalProperties.equals(rhs.additionalProperties))))&&((this.region == rhs.region)||((this.region!= null)&&this.region.equals(rhs.region))));
    }

}
