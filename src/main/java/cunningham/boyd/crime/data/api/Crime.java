package cunningham.boyd.crime.data.api;

import cunningham.boyd.crime.data.model.crime.CrimeCategory;
import cunningham.boyd.crime.data.service.CrimeCategoryService;
import cunningham.boyd.crime.data.service.CrimesAtLocationService;
import cunningham.boyd.crime.data.service.PostcodesService;
import cunningham.boyd.crime.data.support.PropMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.notFound;
import static org.springframework.http.ResponseEntity.ok;

/**
 * A resource that represents data about crimes.
 *
 * @author boyd@sun-certified.com
 */
@RestController
@RequestMapping("/")
public class Crime {

    private static final Logger LOG = LoggerFactory.getLogger(Crime.class);

    @Autowired
    private CrimeCategoryService categoryService;

    @Autowired
    private CrimesAtLocationService locationService;

    @Autowired
    private PostcodesService postcodesService;

    @Autowired
    private PropMapper converter;

    /**
     * Retrieve a set of categories of crimes.
     *
     * @return A representation of the response returned to this endpoint's
     * consumers.
     */
    @GetMapping(value = "crime/categories", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<CrimeCategory> categorize() {

        LOG.info("In categorize()");

        var categorized = categoryService
                .categorize(categoryService.allCategories());

        var categories = new CrimeCategory().withCategories(categorized);

        ResponseEntity<CrimeCategory> response = categorized != null
                && !categorized.isEmpty() ? ok().body(categories)
                : notFound().build();

        return response;

    }

    /**
     * Find crime data based on the given date and location.
     *
     * @param date A date in the format {@code yyyy-mm}.
     * @param postCode A UK postcode.
     * @return A representation of the response returned to this
     * endpoint's consumers.
     */
    @GetMapping(value = "crimes", produces = APPLICATION_JSON_VALUE)
    public ResponseEntity<?> locateCrimes(@RequestParam(required = true,
            name = "date") final String date,
                                          @RequestParam(required = true,
                                                  name = "postcode")
                                                  String postCode) {

        LOG.info("In locateCrimes()");

        postCode = postCode.replaceAll(" ", "");

        var response = ResponseEntity.ok().build();

        if (!postcodesService.validate(postCode).getResult().booleanValue()) {
            return ResponseEntity.badRequest().body(List.of());
        }

        var postCodes = postcodesService.lookup(postCode);

        var latitude = postCodes.getResult().getLatitude();

        var longitude = postCodes.getResult().getLongitude();

        var located = locationService.locateCrimes(date, latitude, longitude);

        located.stream().map(crime -> crime.getCategory()).count();

        var crimes  = located.stream()
                .map(crime -> converter
                        .map(crime,
                                new cunningham.boyd
                                        .crime.data.model.crime.Crime()))
                .collect(Collectors.toList());

        response = !located.isEmpty() ? ok().body(crimes) : notFound().build();

        return response;
    }
}
